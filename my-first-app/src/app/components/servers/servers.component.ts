import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  serverName: string;
  userName: string;
  servers = ['TestServer1', 'Testserver2'];
  clicks = [];
  showDetails: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  unUpdateServerName(e: Event) {
    this.serverName = (<HTMLInputElement>e.target).value;
  }
  
  addServer() {
    this.servers.push(this.serverName);
  }

  toggleDetails(e) {
    this.clicks.push(e);
    this.showDetails = !this.showDetails;
  }
}
