import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  even: number[] = [];
  odd: number[] = [];

  tick(e: number) {
    if(e % 2 === 0) {
      this.even.push(e);
    } else {
      this.odd.push(e);
    }
  }
}
