import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  @Output() tick: EventEmitter<number> = new EventEmitter();
  interval: any;
  counter: number = 0;

  constructor() { }

  ngOnInit() {
  }

  startGame() {
    this.interval = setInterval(() => {
      this.tick.emit(this.counter);
      this.counter ++;
    }, 1000);
  }

  stopGame() {
    clearInterval(this.interval);
  }

}
