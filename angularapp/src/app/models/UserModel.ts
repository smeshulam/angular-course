export interface UserModel {
  firstName: string;
  lastName: string;
  age?: number;
  email: string;
  address?: {
    street?: string;
    city?: string;
    state?: string;
  }
  isActive?: boolean;
  registred?: any;
  showExtended?: boolean;
}