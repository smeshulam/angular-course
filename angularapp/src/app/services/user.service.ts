import { Injectable } from '@angular/core';
import { UserModel } from '../models/UserModel';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: UserModel[];
  data: Observable<any>

  constructor() {
    this.users = [
      {
        firstName: 'shahar',
        lastName: 'meshulam',
        age: 31,
        address: {
          street: 'harimon',
          city: 'ginaton',
          state: 'israel'
        },
        isActive: true,
        registred: new Date('01/02/2019 08:00:00'),
        showExtended: true,
        email: 'shahar@gmail.com'
      },
      {
        firstName: 'Kevin',
        lastName: 'Johnson',
        age: 34,
        address: {
          street: '20 School',
          city: 'Lynn',
          state: 'MA'
        },
        isActive: false,
        registred: new Date('11/02/2019 12:00:00'),
        showExtended: true,
        email: 'Kevin@gmail.com'
      },
      {
        firstName: 'Karen',
        lastName: 'Williams',
        age: 36,
        address: {
          street: '55 Mill st',
          city: 'Miami',
          state: 'MA'
        },
        isActive: true,
        registred: new Date('12/03/2019 14:00:00'),
        showExtended: true,
        email: 'Karen@gmail.com'
      }
    ];
  }

  getData(): Observable<any> {
    this.data = new Observable(observer => {
      setTimeout(() => {
        observer.next(1);
      },1000)

      setTimeout(() => {
        observer.next(2);
      },2000)

      setTimeout(() => {
        observer.next(3);
      },3000)
    })

    return this.data
  }

  getUsers(): Observable<UserModel[]> {
    return of(this.users);
  }

  addUser(user: UserModel) {
    this.users.unshift(user);
  }
}
