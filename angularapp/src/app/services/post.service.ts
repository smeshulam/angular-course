import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../models/Post';


@Injectable({
  providedIn: 'root'
})
export class PostService {
  serverUrl: string = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.serverUrl);
  }

  savePost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.serverUrl, post)
  }

  updatePost(post: Post): Observable<Post> {
    const url = `${this.serverUrl}/${post.id}`;
    return this.http.put<Post>(this.serverUrl, post);
  }

  deletePost(post: Post | number): Observable<Post> {
    const id = typeof post === 'number' ? post : post.id;
    const url = `${this.serverUrl}/${id}`;
    return this.http.delete<Post>(url)
  }

  getPost(id: number): Observable<Post> {
    const url = `${this.serverUrl}/${id}`;
    return this.http.get<Post>(url);
  }

}
