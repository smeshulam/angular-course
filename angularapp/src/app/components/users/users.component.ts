import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/models/UserModel';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: UserModel[] = [];
  showExtended: boolean = true;
  loaded: boolean = false;
  enableAdd: boolean = true;
  showUserForm: boolean = false;
  data: any;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getData().subscribe((data) => {
      console.log(data)
    });
    setTimeout(() => {
      this.userService.getUsers().subscribe((users) => {
        this.users = users
      });
      this.loaded = true;
    }, 2000)
  }

  addUser(f: NgForm) {
    this.userService.addUser({
      firstName: f.value.firstName,
      lastName: f.value.lastName,
      age: f.value.age,
      address: {
        street: f.value.street,
        city: f.value.city,
        state: f.value.state
      },
      isActive: true,
      registred: new Date(),
      showExtended: true,
      email: f.value.email
    });
    f.reset();
  }

  toggleShowExtended(user: UserModel) {
    user.showExtended = !user.showExtended
  }

  toggleUserForm() {
    this.showUserForm = !this.showUserForm;
  }

}
