import { Component, OnInit, Output } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/Post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts: Post[] = [];
  currentPost: Post = {
    id: 0,
    userId: 0,
    title: '',
    body: ''
  };
  isEdit: boolean = false;

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.getPosts().subscribe((posts) => {
      this.posts = posts;
    })
  }

  addPost(post: Post) {
    this.posts.unshift(post)
  }

  editPost(post: Post) {
    this.currentPost = post;
    this.isEdit = true;
  }

  onUpdatePost(post: Post) {
    this.isEdit = false;
    this.currentPost = {
      id: 0,
      userId: 0,
      title: '',
      body: ''
    }
  }

  removePost(post: Post) {
    if (confirm('Are you shure?')) {
      this.postService.deletePost(post.id).subscribe(() => {
        this.posts.forEach((curPost, index) => {
          if (post.id == curPost.id) {
            this.posts.splice(index, 1);
          }
        });
        if(this.currentPost === post) {
          this.currentPost = {
            id: 0,
            userId: 0,
            title: '',
            body: ''
          };
        }
      })
    }
  }

}
