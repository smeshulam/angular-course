import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/Post';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  @Output() addPostEvent: EventEmitter<Post> = new EventEmitter();
  @Output() updatedPost: EventEmitter<Post> = new EventEmitter()
  @Input() currentPost: Post;
  @Input() isEdit: boolean;
  @ViewChild('f', {static: false}) form: NgForm

  constructor(private postService: PostService) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    const post = {
      userId: Math.random(),
      id: Math.random(),
      title: f.value.title,
      body: f.value.body
    } as Post

    if (!this.isEdit) {
      this.addPost(post);
    } else {
      this.updatePost(post);
    }
  }

  addPost(post: Post) {
    this.postService.savePost(post).subscribe(post => {
      this.addPostEvent.emit(post)
      this.form.reset();
    });
  }

  updatePost(post: Post) {
    this.postService.updatePost(post).subscribe(post => {
      this.isEdit = false;
      this.updatedPost.emit(post)
      //f.reset()
    });
  }

}
