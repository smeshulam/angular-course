import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RecipeModel } from '../RecipeModel';

import { ActivatedRoute, Router } from '@angular/router';

import * as fromApp from '../../../store/app.reducer';

import * as RecipesActions from '../store/recipe.actions';

import * as ShoppingListActions from '../../shopping-list/store/shopping-list.actions';

import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  recipe: RecipeModel;
  id: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit() {
    // Subscribe to params change
    this.route.params.pipe(
      map(params => {
        return +params['id'];
      }),
      switchMap(id => {
        this.id = id;
        return this.store.select('recipes');
      }),
      map(recipesState => {
        return recipesState.recipes.find((recipe, index) => {
          return index === this.id;
        });
      })
    )
      .subscribe(recipe => {
        this.recipe = recipe;
      });
  }

  toShoppingList() {
    this.store.dispatch(new ShoppingListActions.AddIngredients(this.recipe.ingredients));
  }

  onDelete() {
    this.store.dispatch(new RecipesActions.DeleteRecipe(this.id));
    this.router.navigate(['../'], { relativeTo: this.route })
  }


}
