import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, Effect, ofType } from '@ngrx/effects';

import * as RecipesActions from './recipe.actions';

import * as fromApp from '../../../store/app.reducer';

import { switchMap, map, withLatestFrom } from 'rxjs/operators';

import { RecipeModel } from '../RecipeModel';
import { Store } from '@ngrx/store';

@Injectable()
export class RecipeEffects {
  private serverUrl: string = 'https://recipe-book-9c507.firebaseio.com/';

  @Effect()
  fetchRecipes = this.actions$.pipe(
    ofType(RecipesActions.FETCH_RECIPES),
    switchMap(() => {
      return this.http.get<RecipeModel[]>(this.serverUrl + 'recipes.json')
    }),
    map(recipes => {
      return recipes.map(recipe => {
        return { ...recipe, ingredients: recipe.ingredients ? recipe.ingredients : [] }
      })
    }),
    map(recipes => {
      return new RecipesActions.SetRecipes(recipes);
    })
  )
  
  @Effect({dispatch: false})
  storeRecipes = this.actions$.pipe(
    ofType(RecipesActions.STORE_RECIPES),
    withLatestFrom(this.store.select('recipes')),
    switchMap(([actionData, recipesState]) => {
      return this.http.put(this.serverUrl + 'recipes.json', recipesState.recipes)
    })
  )

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store<fromApp.AppState>
  ) { }
}