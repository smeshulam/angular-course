import { ActionReducerMap } from '@ngrx/store';
import * as fromShoppingList from '../components/shopping-list/store/shopping-list.reducer';

import * as fromAuth from '../components/auth/store/auth.reducer';

import * as fromRecipes from '../components/recipes/store/recipe.reducer';

export interface AppState {
  shoppingList: fromShoppingList.State;
  recipes: fromRecipes.State;
  auth: fromAuth.State;
}

export const appReducer: ActionReducerMap<AppState> = {
  shoppingList: fromShoppingList.shoppingListReducer,
  recipes: fromRecipes.recipeReducer,
  auth: fromAuth.authReducer
}

