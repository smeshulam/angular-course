// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAN0k4X9QUkc2OE_g2z8TAMcI0Lsp7qGQ8",
    authDomain: "clientpanelprod-24327.firebaseapp.com",
    databaseURL: "https://clientpanelprod-24327.firebaseio.com",
    projectId: "clientpanelprod-24327",
    storageBucket: "clientpanelprod-24327.appspot.com",
    messagingSenderId: "518363860526",
    appId: "1:518363860526:web:51e0c4ddc534193a4d19bf",
    measurementId: "G-NN8FRPS3W3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
