import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ClientModel } from '../models/clientModel';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  clientsCollection: AngularFirestoreCollection<ClientModel>;
  clientDoc: AngularFirestoreDocument<ClientModel>;
  clients: Observable<ClientModel[]>;
  client: Observable<ClientModel>;

  constructor(private afs: AngularFirestore) {
    this.clientsCollection = this.afs.collection('clients', ref => ref.orderBy('lastName', 'asc'))
  }

  getClients(): Observable<ClientModel[]> {
    this.clients = this.clientsCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as ClientModel;
        data.id = action.payload.doc.id;
        return data;
      });
    }));

    return this.clients
  }

  addClient(client: ClientModel) {
    this.clientsCollection.add(client);
  }

  getClient(id: string): Observable<ClientModel> {
    this.clientDoc = this.afs.doc<ClientModel>(`clients/${id}`);
    this.client = this.clientDoc.snapshotChanges().pipe(map(action => {
        const data = action.payload.data() as ClientModel;
        data.id = action.payload.id;
        return data;
    }));
    return this.client;
  }

  updateClient(client: ClientModel) {
    this.clientDoc = this.afs.doc(`clients/${client.id}`);
    this.clientDoc.update(client);
  }

  deleteClient(client: ClientModel) {
    this.clientDoc = this.afs.doc(`clients/${client.id}`);
    this.clientDoc.delete();
  }
}
