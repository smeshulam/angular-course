import { Injectable } from '@angular/core';
import { SettingsModel } from '../models/SettingsModel';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  settings: SettingsModel = {
    allowRegistration: true,
    disableBanlaceOnAddEdit: false,
  }

  constructor() { 
    if(localStorage.getItem('settings')) {
      this.settings = JSON.parse(localStorage.getItem('settings'));
    }
  }

  getSettings() {
    return this.settings;
  }

  setSettings(settings: SettingsModel) {
    this.settings = settings;
    localStorage.setItem('settings', JSON.stringify(this.settings));
  }
}
