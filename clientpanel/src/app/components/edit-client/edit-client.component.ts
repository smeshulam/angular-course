import { Component, OnInit } from '@angular/core';
import { ClientModel } from 'src/app/models/clientModel';
import { ClientService } from 'src/app/services/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {
  id: string;
  client: ClientModel = {
    id: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    balance: null
  };
  loaded: boolean = false;
  disableBalanceOnEdit: boolean =  this.settingsService.getSettings().disableBanlaceOnAddEdit;

  constructor(private clientService:ClientService, private route: ActivatedRoute,private flashMessage: FlashMessagesService, private router: Router, private settingsService: SettingsService) { }

  ngOnInit() {
    // Get id from url
    this.id = this.route.snapshot.params['id'];
    // Get Client
    this.clientService.getClient(this.id).subscribe(res => {
      this.client = res;
      this.loaded = true;
    },
    err => {
      console.log('HTTP Error', err)
      this.loaded = true;
    },
    () => console.log('HTTP request completed.'))
  }

  onSubmit(f: NgForm) {
    //Add new client
    this.clientService.updateClient(f.value);
    //Show message
    this.flashMessage.show('Client Updated', { cssClass: 'alert-success', timeout: 4000 });
    //Redirect to dashboard
    this.router.navigate([`/client/${this.id}`]);
  }

}
