import { Component, OnInit } from '@angular/core';
import { ClientModel } from 'src/app/models/clientModel';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn: boolean;
  loggedInUser: string;
  showRegister: boolean = this.settingService.getSettings().allowRegistration;

  constructor(private authService: AuthService, private flashMessage: FlashMessagesService, private router: Router, private settingService: SettingsService) { }

  ngOnInit() {
    this.authService.getAuth().subscribe((auth) => {
      if (auth) {
        this.isLoggedIn = true;
        this.loggedInUser = auth.email
      } else {
        this.isLoggedIn = false
      }
    })
  }

  logOut() {
    this.authService.logOut();
    this.flashMessage.show('Logged Out', { cssClass: 'alet-success', timeout: 4000 });
    this.router.navigate(['/login'])
  }

}
