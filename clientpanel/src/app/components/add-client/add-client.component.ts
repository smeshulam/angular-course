import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ClientService } from 'src/app/services/client.service';
import { Router } from '@angular/router';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
  disableBalanceOnAdd: boolean = this.settingsService.getSettings().disableBanlaceOnAddEdit;

  constructor(private flashMessage: FlashMessagesService, private clientService: ClientService, private router: Router, private settingsService: SettingsService) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    if (this.disableBalanceOnAdd) {
      f.value.balance = 0;
    }
    //Add new client
    this.clientService.addClient(f.value)
    //Show message
    this.flashMessage.show('Client Added', { cssClass: 'alert-success', timeout: 4000 });
    //Redirect to dashboard
    this.router.navigate(['/']);
  }

}
