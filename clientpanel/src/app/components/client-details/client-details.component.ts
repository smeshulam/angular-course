import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/services/client.service';
import { ClientModel } from 'src/app/models/clientModel';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {
  id: string;
  client: ClientModel;
  hasBalance: boolean = false;
  showBalaceUpdateInput: boolean = false;
  loaded: boolean = false;

  constructor(private clientService: ClientService, private flashMessage: FlashMessagesService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    // Get id from url
    this.id = this.route.snapshot.params['id'];
    // Get Client
    this.clientService.getClient(this.id).subscribe(res => {
      this.client = res;
      if(this.client.balance > 0) {
        this.hasBalance = true;
      }
      this.loaded = true;
    },
    err => {
      console.log('HTTP Error', err)
      this.loaded = true;
    },
    () => console.log('HTTP request completed.'))
  }

  updateBalance() {
    this.clientService.updateClient(this.client);
    this.flashMessage.show('Balance updated', {cssClass: 'alert-success', timeout: 4000});
  }

  deleteClient() {
    if(confirm('Are you shure')) {
      this.clientService.deleteClient(this.client);
      this.flashMessage.show('Client Deleted', {cssClass: 'alert-success', timeout:4000});
      this.router.navigate(['/']);
    }
  }

}
