import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';
import { SettingsModel } from 'src/app/models/SettingsModel';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  settings: SettingsModel = {
    allowRegistration: this.settingsService.getSettings().allowRegistration,
    disableBanlaceOnAddEdit: this.settingsService.getSettings().disableBanlaceOnAddEdit
  }

  constructor(private settingsService: SettingsService, private flashMessage: FlashMessagesService) { }

  ngOnInit() {
    this.settingsService.getSettings().allowRegistration
  }

  onSubmit() {
    this.settingsService.setSettings(this.settings);
    this.flashMessage.show('Settings Saved', { cssClass: 'alert-success', timeout: 4000 })
  }

}
