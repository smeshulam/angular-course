import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { RecipeModel } from '../RecipeModel';
import { Subscription } from 'rxjs';

import * as fromApp from '../../../store/app.reducer';

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  recipes: RecipeModel[];
  recipesChangedSb: Subscription;

  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit() {
    this.recipesChangedSb = this.store.select('recipes')
      .pipe(
        map(recipesState => recipesState.recipes
        )
      )
      .subscribe((recipes: RecipeModel[]) => {
        this.recipes = recipes;
      })
  }

  ngOnDestroy() {
    this.recipesChangedSb.unsubscribe();
  }

}
