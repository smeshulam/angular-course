import { createAction, props } from '@ngrx/store';
import { RecipeModel } from '../RecipeModel';


export const addRecipe = createAction(
  '[Recipe] Add Recipe',
  props<{ recipe: RecipeModel }>()
);
export const updateRecipe = createAction(
  '[Recipe] Update Recipe',
  props<{ index: number, recipe: RecipeModel }>()
);
export const deleteRecipe = createAction(
  '[Recipe] Delete Recipe',
  props<{ index: number }>()
);
export const setRecipes = createAction(
  '[Recipe] Set Recipes',
  props<{ recipes: RecipeModel[] }>()
);
export const fetchRecipes = createAction(
  '[Recipe] Fetch Recipes');
export const storeRecipes = createAction(
  '[Recipe] Store Recipes');