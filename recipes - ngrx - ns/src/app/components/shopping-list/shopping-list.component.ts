import { Component, OnInit, OnDestroy } from '@angular/core';
import { IngredientModel } from 'src/app/shared/ingredientModel';
import { Observable } from 'rxjs';
import { LoggingService } from 'src/app/logging.service';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';
import * as ShoppingListActions from './store/shopping-list.actions';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
})
export class ShoppingListComponent implements OnInit {
  ingredients: Observable<{ingredients: IngredientModel[]}>;
  //private ingredientAddedSub: Subscription;
  //private ingredientSelectionChangedSb: Subscription;
  selectedIngredientIndex: number;


  constructor(
    private loggingService: LoggingService,
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit() {
    this.ingredients = this.store.select('shoppingList');
 
    this.loggingService.printLog('Hello from ShoppingListComponent ngOnInit');
  }

  onIngredientSelected(index: number) {
    
    this.store.dispatch(ShoppingListActions.startEdit({index}));
  }
}
