import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { IngredientModel } from 'src/app/shared/ingredientModel';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import * as ShoppingListActions from '../store/shopping-list.actions';

import * as fromApp from '../../../store/app.reducer';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  @ViewChild('f', { static: false }) form: NgForm;
  editMode: boolean;
  private subscription: Subscription;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.subscription = this.store.select('shoppingList').subscribe(stateData => {
      const index = stateData.editIndex;
      if(index > -1) {
        this.editMode = true;
         this.form.setValue({
          'name': stateData.ingredients[index].name,
          'amount': stateData.ingredients[index].amount
        })
      } else {
        this.editMode = false;
      }
    });
  }

  onSubmit(f: NgForm) {
    const newIngredient: IngredientModel = <IngredientModel>f.value;
    if (!this.editMode) {
      this.store.dispatch(ShoppingListActions.addIngredient({ingredient: newIngredient}))
    } else {
      //If in edit mode update ingredient
      this.store.dispatch(ShoppingListActions.updateIngredient({ingredient: newIngredient}));
      //Reset form
    }
    this.onFormClear();
  }

  onDelete() {
    this.store.dispatch(ShoppingListActions.deleteIngredient());
    this.onFormClear();
  }

  onFormClear() {
    //Reset form
    this.form.reset();
    if (this.editMode === true) {
      //Turn off edit mode
      this.editMode = false;
      this.store.dispatch(ShoppingListActions.stopEdit());
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.store.dispatch(ShoppingListActions.stopEdit());
  }
}
