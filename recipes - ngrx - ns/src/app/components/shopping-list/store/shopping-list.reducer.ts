import { Action, createReducer, on } from '@ngrx/store';
import * as ShoppingListActions from './shopping-list.actions';
import { IngredientModel } from 'src/app/shared/ingredientModel';

export interface State {
  ingredients: IngredientModel[];
  editIndex: number;
}
const initialState: State = {
  ingredients: [new IngredientModel('Apples', 5),
  new IngredientModel('Tomatoes', 10)], editIndex: -1
};

export function shoppingListReducer(shoppingListState: State | undefined, shoppingListAction: Action) {
  return createReducer(
    initialState,
    on(ShoppingListActions.addIngredient,
      (state, action) => ({
        ...state,
        ingredients: state.ingredients.concat(action.ingredient)
      })
    ),
    on(ShoppingListActions.addIngredients,
      (state, action) => ({
        ...state,
        ingredients: state.ingredients.concat(...action.ingredients)
      })
    ),
    on(ShoppingListActions.updateIngredient,
      (state, action) => ({
        ...state,
        editIndex: -1,
        ingredients: state.ingredients
          .map((ingredient, index) => index === state.editIndex ? { ...action.ingredient } : ingredient)
      })
    ),
    on(ShoppingListActions.deleteIngredient,
      state => ({
        ...state,
        editIndex: -1,
        ingredients: state.ingredients.filter((ingredient, index) => index !== state.editIndex)
      })
    ),
    on(ShoppingListActions.startEdit,
      (state, action) => ({
        ...state,
        editIndex: action.index
      })
    ),
    on(ShoppingListActions.stopEdit,
      state => ({
        ...state,
        editIndex: -1
      })
    )
  )(shoppingListState, shoppingListAction);
}