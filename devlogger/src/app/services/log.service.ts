import { Injectable } from '@angular/core';
import { LogModel } from '../models/LogModel';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  logs: LogModel[];

  private logSource = new BehaviorSubject<LogModel>({
    id: null,
    text: '',
    date: null
  });

  selectedLog = this.logSource.asObservable();

  constructor() { }

  addLog(log: LogModel) {
    this.logs.unshift(log);
    //Add to local storage
    localStorage.setItem('logs', JSON.stringify(this.logs));
  }

  updateLog(log: LogModel) {
    this.logs.forEach((curLog, index) => {
      if (log.id == curLog.id) {
        this.logs.splice(index, 1);
      }
    });
    this.logs.unshift(log);
    localStorage.setItem('logs', JSON.stringify(this.logs));
  }

  getLogs(): Observable<LogModel[]> {
    if(localStorage.getItem('logs') === null) {
      this.logs = [];
    } else {
      this.logs = JSON.parse(localStorage.getItem('logs'));
    }
    //sort by date
    return of(this.logs.sort((a, b) => {
      return b.date = a.date;
    }));
  }

  setSelectedLog(log: LogModel) {
    this.logSource.next(log);
  }

  deleteLog(log: LogModel) {
    this.logs.forEach((curLog, index) => {
      if (log.id === curLog.id) {
        this.logs.splice(index, 1)
      }
    })
    localStorage.setItem('logs', JSON.stringify(this.logs));
  }
}
