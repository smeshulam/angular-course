export interface LogModel {
  id: string;
  text: string;
  date: any;
}