import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LogService } from 'src/app/services/log.service';
import { LogModel } from 'src/app/models/LogModel';

@Component({
  selector: 'app-log-form',
  templateUrl: './log-form.component.html',
  styleUrls: ['./log-form.component.css']
})
export class LogFormComponent implements OnInit {
  formLog: LogModel = {
    id: null,
    text: '',
    date: null
  };
  isNew: boolean = true;
  @ViewChild('f', { static: false }) f: NgForm;

  constructor(private logService: LogService) { }

  ngOnInit() {
    this.logService.selectedLog.subscribe(log => {
      if (log.id !== null) {
        this.isNew = false;
        this.formLog.id = log.id;
        this.formLog.text = log.text;
        this.formLog.date = log.date;
      }
    });
  }

  onSubmit(f: NgForm) {
    if (this.isNew) {
      //create a new log
      const newLog = {
        id: this.uuidv4(),
        text: f.value.text,
        date: new Date()
      } as LogModel;

      // add log
      this.logService.addLog(newLog);
    } else {
      const updLog = {
        id: this.formLog.id,
        text: this.formLog.text,
        date: new Date()
      };
      this.logService.updateLog(updLog);
      this.isNew = true;
    }
    f.reset();
  }

  onClear() {
    this.f.reset();
    this.logService.setSelectedLog({
      id: null,
      text: '',
      date: null
    });
    this.isNew = true;
  }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

}
