import { Component, OnInit } from '@angular/core';
import { LogModel } from 'src/app/models/LogModel';
import { LogService } from 'src/app/services/log.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {
  logs: LogModel[] = [];
  selectedLog: LogModel;
  loaded: boolean = false;

  constructor(private logService: LogService) { }

  ngOnInit() {
    this.logService.getLogs().subscribe(logs => {
      this.logs = logs;
      this.loaded = true;
    });
    this.logService.selectedLog.subscribe(log => this.selectedLog = log)
  }

  onSelect(log: LogModel) {
    if (log === this.selectedLog) {
      this.logService.setSelectedLog({
        id: '',
        text: '',
        date: null
      });
    } else {
      this.logService.setSelectedLog(log);
      this.selectedLog = log;
    }
  }

  deleteLog(log: LogModel) {
    this.logService.deleteLog(log)
  }

}
