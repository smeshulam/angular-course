import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";

export class CustomValidators {
  static allowedProjectNames(control: FormControl): Promise<any> | Observable<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if(control.value === 'test') {
          resolve({'nameNotAllowed': true});
        } else {
          resolve(null)
        }
      }, 1500)
    })
  }
}