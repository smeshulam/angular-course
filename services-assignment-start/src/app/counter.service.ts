import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CounterService {
  setToInactiveCounter: number = 0;
  setToActiveCounter: number = 0;

  constructor() { }

  countSetToInactive() {
    this.setToInactiveCounter ++;
  }
  countSetToActive() {
    this.setToActiveCounter ++;
  }
}
