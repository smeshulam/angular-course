import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RecipeService } from '../components/recipes/recipe.service';
import { RecipeModel } from '../components/recipes/RecipeModel';

import { map, tap, take, exhaustMap } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class DataStorageService {
  private serverUrl: string = 'https://recipe-book-9c507.firebaseio.com/';

  constructor(private http: HttpClient, private recipeService: RecipeService, private authService: AuthService) { }

  storeRecipes() {
    const recipes = this.recipeService.getRecipes();
    this.http.put(this.serverUrl + 'recipes.json', recipes).subscribe(response => {
      console.log(response);
    })
  }

  fetchRecipes() {
    return this.http.get<RecipeModel[]>(this.serverUrl + 'recipes.json')
      .pipe(
        map(recipes => {
          return recipes.map(recipe => {
            return { ...recipe, ingredients: recipe.ingredients ? recipe.ingredients : [] }
          })
        }),
        tap(recipes => {
          this.recipeService.setRecipes(recipes);
        })
      )



  }

}