import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataStorageService } from 'src/app/shared/data-storage.service';
import { AuthService } from 'src/app/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuthenticated: boolean = false;
  private authSb: Subscription;

  constructor(private dataService: DataStorageService, private authService: AuthService) { }

  ngOnInit() {
    this.authSb = this.authService.user.subscribe(user => {
      this.isAuthenticated = !!user; //!user ? false : true;
    });
  }

  onSaveData() {
    this.dataService.storeRecipes();
  }

  onLogout() {
    this.authService.logout();
  }

  onFetchData() {
    this.dataService.fetchRecipes().subscribe();
  }

  ngOnDestroy() {
    this.authSb.unsubscribe();
  }

}
