import { Component, OnInit, OnDestroy } from '@angular/core';
import { IngredientModel } from 'src/app/shared/ingredientModel';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs';
import { LoggingService } from 'src/app/logging.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  ingredients: IngredientModel[];
  private ingredientAddedSub: Subscription;
  private ingredientSelectionChangedSb: Subscription;
  selectedIngredientIndex: number;


  constructor(private shoppingListService: ShoppingListService, private loggingService: LoggingService) { }

  ngOnInit() {
    this.ingredients = this.shoppingListService.getIngredients();
    //Subscribe to ingridient change
    this.ingredientAddedSub = this.shoppingListService.ingredientChange.subscribe((ingredients: IngredientModel[]) => this.ingredients = ingredients)
    //Subscribe to ingridient selection change
    this.ingredientSelectionChangedSb = this.shoppingListService.ingredientSelected.subscribe((ingredientIndex: number) => {
      this.selectedIngredientIndex = ingredientIndex;
    });

    this.loggingService.printLog('Hello from ShoppingListComponent ngOnInit');
  }

  onIngredientSelected(ingredientIndex: number) {
    //Toggle ingredient selected
    if (this.selectedIngredientIndex !== ingredientIndex) {
      this.selectedIngredientIndex = ingredientIndex;
      this.shoppingListService.setSelectedIngredient(ingredientIndex);
    } else {
      this.selectedIngredientIndex = -1;
      this.shoppingListService.setSelectedIngredient(this.selectedIngredientIndex);
    }
  }

  ngOnDestroy() {
    this.ingredientAddedSub.unsubscribe();
    this.ingredientSelectionChangedSb.unsubscribe();
  }
}
