import { Injectable } from '@angular/core';
import { IngredientModel } from 'src/app/shared/ingredientModel';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {
  private ingredients: IngredientModel[] = [
    new IngredientModel('Apples', 5),
    new IngredientModel('Tomatos', 10)
  ];

  ingredientSelected: Subject<number> = new Subject();
  ingredientChange: Subject<IngredientModel[]> = new Subject();

  constructor() { }

  getIngredients(): IngredientModel[] {
    return this.ingredients.slice();
  }

  addIngredient(ingredient: IngredientModel) {
    this.ingredients.unshift(ingredient);
    return this.ingredientChange.next(this.ingredients.slice());
  }

  addIngredients(ingredients: IngredientModel[]) {
    this.ingredients.unshift(...ingredients);
    this.ingredientChange.next(this.ingredients.slice());
  }

  setSelectedIngredient(ingredientIndex: number) {
    this.ingredientSelected.next(ingredientIndex);
  }

  getIngredient(ingredientIndex): IngredientModel {
    return this.ingredients.slice()[ingredientIndex];
  }

  updateIngredient(index: number, ingredient: IngredientModel) {
    this.ingredients[index] = ingredient;
    this.ingredientChange.next(this.ingredients.slice());
  }

  deleteIngredient(index: number) {
    this.ingredients.splice(index, 1);
    this.ingredientChange.next(this.ingredients.slice());
  }
 
}
