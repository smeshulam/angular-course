import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { IngredientModel } from 'src/app/shared/ingredientModel';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  @ViewChild('f', { static: false }) form: NgForm;
  editMode: boolean;
  private selectedIngredientSb: Subscription;
  private selectedIngredientIndex: number;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    //subscribe selected ingredient
    this.selectedIngredientSb = this.shoppingListService.ingredientSelected.subscribe((ingredientIndex: number) => {
      this.selectedIngredientIndex = ingredientIndex;

      if (ingredientIndex > -1) {
        //on selected ingredient change get ingredient
        const ingredient: IngredientModel = this.shoppingListService.getIngredient(ingredientIndex);

        //Set form value to selected ingredient
        this.form.setValue({
          'name': ingredient.name,
          'amount': ingredient.amount
        })

        //Turn on edit mode
        this.editMode = true;
      } else {
        //If deselect ingredient - reset form
        this.form.reset();

        //Turn off edit mode
        this.editMode = false;
      }

    })
  }

  onSubmit(f: NgForm) {
    if (!this.editMode) {
      const newIngredient: IngredientModel = <IngredientModel>f.value;
      this.shoppingListService.addIngredient(newIngredient);
    } else {
      //If in edit mode update ingredient
      this.shoppingListService.updateIngredient(this.selectedIngredientIndex, this.form.value);
      //Reset form
    }
    this.onFormClear();
  }

  onDelete() {
    this.shoppingListService.deleteIngredient(this.selectedIngredientIndex);
    this.onFormClear();
  }

  onFormClear() {
    //Reset form
    this.form.reset();
    if (this.editMode === true) {
      //Turn off edit mode
      this.editMode = false;
      //Change selected Ingredient to nothing
      this.shoppingListService.setSelectedIngredient(-1);
    }
  }

  ngOnDestroy() {
    this.selectedIngredientSb.unsubscribe();
  }
}
