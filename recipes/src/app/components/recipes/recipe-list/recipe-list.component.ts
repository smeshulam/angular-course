import { Component, OnInit, OnDestroy } from '@angular/core';
import { RecipeModel } from '../RecipeModel';
import { RecipeService } from '../recipe.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  recipes: RecipeModel[];
  recipesChangedSb: Subscription;

  constructor(private recipeService: RecipeService) { }

  ngOnInit() {
    this.recipes = this.recipeService.getRecipes();
    this.recipesChangedSb = this.recipeService.recipesChaned.subscribe((recipes: RecipeModel[]) => {
      this.recipes = recipes;
    })
  }

  ngOnDestroy() {
    this.recipesChangedSb.unsubscribe();
  }

}
