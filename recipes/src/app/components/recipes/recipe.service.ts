import { Injectable } from '@angular/core';
import { RecipeModel } from './RecipeModel';
import { IngredientModel } from 'src/app/shared/ingredientModel';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  recipesChaned: Subject<RecipeModel[]> = new Subject();

  // private recipes: RecipeModel[] = [
  //   new RecipeModel(
  //     'A Test Recipe',
  //     'This is a simple test',
  //     'https://ichef.bbci.co.uk/food/ic/food_16x9_1600/recipes/chicken_with_red_kidney_97909_16x9.jpg',
  //     [
  //       new IngredientModel('Milk', 0.5),
  //       new IngredientModel('Butter', 1)
  //     ]),
  //   new RecipeModel(
  //     'A Test Recipe2',
  //     'This is a simple test',
  //     'https://ichef.bbci.co.uk/food/ic/food_16x9_1600/recipes/chicken_with_red_kidney_97909_16x9.jpg',
  //     [
  //       new IngredientModel('Nuts', 5),
  //       new IngredientModel('Flour', 0.5)
  //     ])
  // ];

  private recipes: RecipeModel[] = [];

  private selectedRecipe: RecipeModel;

  constructor(private shoppingListService: ShoppingListService) { }

  getRecipes(): RecipeModel[] {
    return this.recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: IngredientModel[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

  setRecipes(recipes: RecipeModel[]) {
    this.recipes = recipes;
    this.recipesChaned.next(this.recipes.slice());
  }

  getRecipe(id: number): RecipeModel {
    return this.recipes[id];
  }

  addRecipe(recipe: RecipeModel) {
    this.recipes.unshift(recipe);
    this.recipesChaned.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: RecipeModel) {
    this.recipes[index] = newRecipe;
    this.recipesChaned.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChaned.next(this.recipes.slice());
  }
}
